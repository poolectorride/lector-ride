
package lectorride;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JOptionPane;

public class Factura {
    //Sobre factura
    private String numAutorizacion;
    private String numeroFactura;
    private String claveAcceso;
    private String fecha;
    
    //totales
    private double totalSinIva;
    private double totalIva;
    private double totalFactura;
    
    //productos
    private String [] productos;
    private double [] precioSinIva;
    private boolean [] iva;
        
    //Sobre empresa
    private String nombreComercial;
    private String razonSocial;
    private String direccionEmpresa;
    private String rucEmpresa;
    
    //Sobre el cliente
    private String rucCliente;
    private String nombreCliente;
    private String direccionCliente;
    
    //otros datos
    
    private String textoFactura;
    
    //metodos
    public void cargarFactura (String direccionFactura){
        //Recibe la direccion de un archivo .xml de factura electronica y lo convierte en string
        try {
            File factura = new File(direccionFactura);
            FileReader fr = new FileReader(factura);
            BufferedReader br = new BufferedReader(fr);
            String linea;
            textoFactura = "";
            while((linea=br.readLine())!=null){
                textoFactura = textoFactura + " "+ linea;
            }
            
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Un error inesperado ha ocurrido "
                    + "\nAdicionalmente este mensaje fue entregado: "+ e,"Error de carga",JOptionPane.ERROR_MESSAGE);
        }
        
    }
    
    public boolean estaAutorizado (){
        /*
        Retorna verdadero si la factura fue autorizada por el SRI
        Retorna falso si no ha sido autorizada por el SRI
        Importante: Debe cargarse una factura con el metodo cargarFactura primero
        */
        boolean resultado = false;
        int empieza = textoFactura.indexOf("<estado>") + "<estado>".length();
        int Final = textoFactura.indexOf("</estado>", empieza);
        String texto = "";
        if (Final>0){
            texto = textoFactura.substring(empieza, Final);
        }
        
        if (texto.equals("AUTORIZADO")){
            resultado = true;
        }
        return resultado;
    }
    
    
    
}
